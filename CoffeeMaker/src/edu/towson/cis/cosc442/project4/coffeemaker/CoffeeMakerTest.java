package edu.towson.cis.cosc442.project4.coffeemaker;

import org.ietf.jgss.Oid;

import junit.framework.TestCase;

/**
 *
 */
public class CoffeeMakerTest extends TestCase {
	private CoffeeMaker cm;
	private Inventory i;
	private Recipe r1;

	public void setUp() {
		cm = new CoffeeMaker();
		i = cm.checkInventory();

		r1 = new Recipe();
		r1.setName("Coffee");
		r1.setPrice(50);
		r1.setAmtCoffee(6);
		r1.setAmtMilk(1);
		r1.setAmtSugar(1);
		r1.setAmtChocolate(0);
	}

	public void testAddRecipe1() {
		assertTrue(cm.addRecipe(r1));
	}

	public void testDeleteRecipe1() {
		cm.addRecipe(r1);
		assertTrue(cm.deleteRecipe(r1));
	}

	public void testEditRecipe1() {
		cm.addRecipe(r1);
		Recipe newRecipe = new Recipe();
		newRecipe = r1;
		newRecipe.setAmtSugar(2);
		assertTrue(cm.editRecipe(r1, newRecipe));
	}
	
	public void testcheckOptions() {
		//Main.mainMenu(); was causing coverage issues
	}
	
	public void testaddInventory1() {
		cm.addInventory(1, 1, 1, 1);
	}
	
	public void testcheckInventory() {
		assertEquals(cm.checkInventory(), cm.checkInventory());
	}
	
	public void testPurchaseBeverage1() {
		assertEquals(50, cm.makeCoffee(r1, 100));
	}
	
	public void testRecipeCoverage() { //completes coverage of recipe to beyond 90%
		r1.setAmtChocolate(-5);
		r1.setAmtCoffee(-5);
		r1.setAmtMilk(-5);
		r1.setAmtSugar(-5);
		r1.setPrice(-5);
		
		//now for coverage for mutation testing
		Recipe r2 = new Recipe();
		r2.setName("Milk");
		r2.setPrice(50);
		r2.setAmtCoffee(1);
		r2.setAmtMilk(1);
		r2.setAmtSugar(1);
		r2.setAmtChocolate(1);
		
		assertEquals(1, r2.getAmtChocolate());
		assertEquals(1, r2.getAmtCoffee());
		assertEquals(1, r2.getAmtMilk());
		assertEquals(1, r2.getAmtSugar());
		
		r2.setAmtCoffee(0);
		r2.setAmtMilk(0);
		r2.setAmtSugar(0);
		r2.setAmtChocolate(0);
		
		assertEquals(0, r2.getAmtChocolate());
		assertEquals(0, r2.getAmtCoffee());
		assertEquals(0, r2.getAmtMilk());
		assertEquals(0, r2.getAmtSugar());
		
		assertEquals("Milk", r2.toString());
	}
	
	public void testInventoryCoverage() { //coverage for inventory
		i.setChocolate(-5);
		i.setCoffee(-5);
		i.setMilk(-5);
		i.setSugar(-5);
		assertEquals("Coffee: 0\n"
				+ "Milk: 0\n"
				+ "Sugar: 0\n"
				+ "Chocolate: 0\n", i.toString());
		
		//now begins coverage based around mutation testing
		Inventory i2 = new Inventory();
		
		assertEquals(15, i2.getChocolate());
		i2.setChocolate(-5);
		assertEquals(0, i2.getChocolate());
		
		assertEquals(15, i2.getCoffee());
		i2.setCoffee(-5);
		assertEquals(0, i2.getCoffee());
		
		assertEquals(15, i2.getMilk());
		i2.setMilk(-5);
		assertEquals(0, i2.getMilk());
		
		assertEquals(15, i2.getSugar());
		i2.setSugar(-5);
		assertEquals(0, i2.getSugar());
		
		assertEquals(false, i2.enoughIngredients(r1));
		
		i2.setChocolate(1);
		assertEquals(1, i2.getChocolate());
		
		i2.setCoffee(1);
		assertEquals(1, i2.getCoffee());
		
		i2.setMilk(1);
		assertEquals(1, i2.getMilk());
		
		i2.setSugar(1);
		assertEquals(1, i2.getSugar());
		
		i2.setChocolate(100);
		i2.setCoffee(100);
		i2.setMilk(100);
		i2.setSugar(100);
		assertEquals(true, i2.enoughIngredients(r1));
	}
	
	public void testCoffeeMakerCoverage() { //coverage for coffee maker 
		assertEquals(true, cm.addRecipe(r1));
		assertEquals(false, cm.addRecipe(r1));
		assertEquals(false, cm.addInventory(-5, -5, -5, -5));
		assertEquals(r1, cm.getRecipeForName(r1.getName()));
		cm.getRecipes();
		assertEquals(true, cm.addInventory(5, 5, -5, 5));
		//now for attempts at mutation coverage....
		cm.deleteRecipe(null);
		Recipe r2 = new Recipe();
		assertEquals(false, cm.editRecipe(r2, r2));
		CoffeeMaker cm2 = new CoffeeMaker();
		cm2 = cm;
		assertEquals(cm2.getRecipes(), cm.getRecipes());
		Recipe ra = new Recipe();
		CoffeeMaker cm3 = new CoffeeMaker();
		cm3.addRecipe(r1);
		cm3.addRecipe(r2);
		
		ra = new Recipe();
		ra.setName("Milk");
		ra.setPrice(51);
		ra.setAmtCoffee(7);
		ra.setAmtMilk(2);
		ra.setAmtSugar(2);
		ra.setAmtChocolate(0);
		cm3.addRecipe(ra);
		
		Recipe rb = new Recipe();
		rb.setName("Milk");
		rb.setPrice(51);
		rb.setAmtCoffee(7);
		rb.setAmtMilk(2);
		rb.setAmtSugar(2);
		rb.setAmtChocolate(0);
		cm3.addRecipe(rb);
		
		//due to problems with this test, we had to make the recipes public in order to manually set them for mutation
		
		Recipe[] rArray = {r1, r2, ra, rb};
		cm3.recipeArray = rArray;
		assertEquals(rArray, cm3.getRecipes());
		assertEquals(2, cm.makeCoffee(r1, 2));
		
		assertEquals(true, cm2.deleteRecipe(r1));
		assertEquals(false, cm2.deleteRecipe(rb));
		
		cm2.addInventory(3, 3, -500, 3);
		assertEquals(200, cm2.makeCoffee(r1, 200)); //should return change because not enough sugar
	}
	
}